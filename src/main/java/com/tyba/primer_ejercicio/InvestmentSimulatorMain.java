package com.tyba.primer_ejercicio;

import static com.tyba.primer_ejercicio.enums.DefinitionRangsInvestmentSimulator.*;
import com.tyba.primer_ejercicio.utilities.ConvertCurrencyFormat;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;

import java.io.File;
import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;

public class InvestmentSimulatorMain implements Initializable {
    @FXML
    private Label resultadoSimulacion;
    @FXML
    private ComboBox<String> tipoDocumento;
    @FXML
    private ImageView imageTyba;
    @FXML
    private TextField ingresoNombre;
    @FXML
    private TextField montoInvertir;

    @Override
    public void initialize(URL url, ResourceBundle rb){
        tipoDocumento.setItems(FXCollections.
                observableArrayList("Cédula de Ciudadanía","Cédula de Extranjería","Pasaporte",
                        "Permisos especial de permanencia", "Registro civil", "Tarjeta de identidad", "Otro"));
        File file = new File("src/main/resources/com/tyba/primer_ejercicio/Images/logo-Tyba.png");
        Image image = new Image(file.toURI().toString());
        imageTyba.setImage(image);
        imageTyba.setPreserveRatio(true);
    }

    @FXML
    protected void onContinuar() {
        if (ingresoNombre.getText().equals("") || montoInvertir.getText().equals("") || tipoDocumento.getValue() == null){
            resultadoSimulacion.
                    setText("El nombre, tipo de documento o monto se encuentra vacio, valide la informacion");
            resultadoSimulacion.setTextFill(Color.RED);
        }else if (tipoDocumento.getValue().equals("Otro")){
            resultadoSimulacion.
                    setText("Cualquier otro documento diferente a los de la lista no es valido");
            resultadoSimulacion.setTextFill(Color.RED);
        }else if (!Objects.equals(tipoDocumento.getValue(), "Cédula de Ciudadanía") && !Objects.equals(tipoDocumento.getValue(), "Cédula de Extranjería")){
            resultadoSimulacion.
                    setText("Gracias por su interés en realizar una inversion, pero en el momento no puedes invertir debido a tu tipo de documento");
            resultadoSimulacion.setTextFill(Color.RED);
        }else if (Integer.parseInt(montoInvertir.getText())<MONTOMINIMO.value() || Integer.
                parseInt(montoInvertir.getText())>MONTOMAXFONDOINV.value()) {
            resultadoSimulacion.
                    setText("El monto ingresado esta fuera de rango");
            resultadoSimulacion.setTextFill(Color.RED);
        }else if (Integer.parseInt(montoInvertir.getText()) < MONTOMINIMOCDT.value()){
            resultadoSimulacion.
                    setText("El monto minimo para realizar una inversion es $"+ ConvertCurrencyFormat.
                            convertCurrencyFormat(MONTOMINIMOCDT.value()));
            resultadoSimulacion.setTextFill(Color.RED);
        } else {
            resultadoSimulacion.setText(InvestmentSimulatorValidations.
                    validatiosSimulator(ingresoNombre.getText(),Integer.parseInt(montoInvertir.getText())));
            resultadoSimulacion.setTextFill(Color.BLACK);
        }
    }

}