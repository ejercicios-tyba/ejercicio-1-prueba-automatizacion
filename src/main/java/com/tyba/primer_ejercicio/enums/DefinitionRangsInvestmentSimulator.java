package com.tyba.primer_ejercicio.enums;

public enum DefinitionRangsInvestmentSimulator {

    //Monto minimo para poder invertir y monto minimo para mostrar la recomendacion del producto CDT
    MONTOMINIMOCDT(200000),
    //Monto maximo para mostrar la recomendacion del producto CDT y minimo para mostrar la recomendacion del producto Acciones
    MONTOMAXCDTMINACCIONES(5000000),
    //Monto maximo para mostrar la recomendacion del producto Acccciones y minimo para mostrar la recomendacion del producto Fondo de inversion
    MONTOMAXACCIONESMINFONDOINV(25000000),
    //Monto maximo para mostrar la recomendacion del producto Fondo de Inversion y maximo monto para poder invertir
    MONTOMAXFONDOINV(300000000),
    //Monto minimo que se puede ingresar
    MONTOMINIMO(0);


    private final int value;
    DefinitionRangsInvestmentSimulator(int value) {this.value = value;}
    public int value() {
        return value;
    }

}
