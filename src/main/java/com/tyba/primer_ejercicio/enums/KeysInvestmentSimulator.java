package com.tyba.primer_ejercicio.enums;

public enum KeysInvestmentSimulator {
    //Ancho de ventana
    WIDTH(500),
    //Alto de ventana
    HEIGTH(550);


    private final int value;
    KeysInvestmentSimulator(int value) {this.value = value;}
    public int value() {
        return value;
    }
}
