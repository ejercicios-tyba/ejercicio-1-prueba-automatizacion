package com.tyba.primer_ejercicio;

import static com.tyba.primer_ejercicio.enums.DefinitionRangsInvestmentSimulator.*;
import com.tyba.primer_ejercicio.utilities.ConvertCurrencyFormat;

public class InvestmentSimulatorValidations {

    private InvestmentSimulatorValidations(){}
    public static String validatiosSimulator(String nombre, int monto){
        String respuesta = "";

        if (monto>= MONTOMINIMOCDT.value() && monto< MONTOMAXCDTMINACCIONES.value()){
            respuesta = ", se le recomienda adquirir el producto CDT el cual tendra una tasa de ganacia del 7%, sin importar el plazo o algun otro factor";
        }
        if (monto>=MONTOMAXCDTMINACCIONES.value() && monto< MONTOMAXACCIONESMINFONDOINV.value()){
            respuesta = ", se le recomienda adquirir el producto Acciones el cual tendra una tasa de ganacia del 12%, sin importar el plazo o algun otro factor";
        }
        if (monto>= MONTOMAXACCIONESMINFONDOINV.value() && monto<=MONTOMAXFONDOINV.value()){
            respuesta = ", se le recomienda adquirir el producto Fondos de Inversión el cual tendra una tasa de ganacia del 20%, sin importar el plazo o algun otro factor";
        }

        return "Señor(a) "+nombre+" acorde al monto que ingreso $"+ ConvertCurrencyFormat.convertCurrencyFormat(monto) +respuesta;
    }
}
