package com.tyba.primer_ejercicio.utilities;

import java.text.DecimalFormat;

public class ConvertCurrencyFormat {

    private ConvertCurrencyFormat(){}
    public static String convertCurrencyFormat(int monto){
        DecimalFormat formato = new DecimalFormat("#,###.###");
        return formato.format(monto);
    }

}
