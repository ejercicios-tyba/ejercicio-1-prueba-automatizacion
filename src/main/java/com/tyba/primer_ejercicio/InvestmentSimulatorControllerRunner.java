package com.tyba.primer_ejercicio;

import com.tyba.primer_ejercicio.enums.KeysInvestmentSimulator;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class InvestmentSimulatorControllerRunner extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(InvestmentSimulatorControllerRunner.class.getResource("InvestmetSimulatorView.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), KeysInvestmentSimulator.WIDTH.value(),
                KeysInvestmentSimulator.HEIGTH.value());
        stage.setTitle("Simulador de Inversión Tyba");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}