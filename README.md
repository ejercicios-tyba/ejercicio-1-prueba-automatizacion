# Ejercicio 1 Prueba Automatizacion TYBA

Este repositorio cuenta con el primer ejercicio de la prueba conocimientos de automatizacion TYBA el cual se ha desarrollado con JAVAFX, Gradle, entre otros.
Se realizo con JAVAFX de tal forma que se tenga una interfaz grafica de usuario. 

## Pre-requisitos 📋

Necesitas tener previamente instalado en tu maquina:

* [Git](https://git-scm.com/downloads) - El manejador de versiones
* [IntelliJ](https://www.jetbrains.com/es-es/idea/download/#section=windows) - El IDe De preferencia
* [Java 11](https://www.oracle.com/java/technologies/javase/jdk11-archive-downloads.html) - El JDK para correr el robot
* [Gradle](https://gradle.org/install/) - El Gestor de dependencias


## Comenzando 🚀

Para obtener una copia del programa, basta con abrir una consola CMD o Power Shell para Windows, o para Linux y Mac OS utilizar la consola de comandos en una carpeta o en el lugar en el cual desea guardar el proyecto y a continuacion ejecutar el siguente comando git:

git clone https://gitlab.com/ejercicios-tyba/ejercicio-1-prueba-automatizacion.git

### Configuración 🔧

* En **IntelliJ** se debe instalar y validar que esten habilitados los plugins para que pueda leer y ejecutar el programa; Vamos al apartado de plugins de IntelliJ y buscamos e instalamos desde el marketplace de intelliJ los siguientes plugins:

  _- Gradle (Normalmente viene instalado por defecto, pero algunas veces no es asi, por ende hay que revisar, adicionalmente validar que este habilitado.)_

  _- JavaFX (Normalmente viene instalado por defecto, pero algunas veces no es asi, por ende hay que revisar, adicionalmente validar que este habilitado.)_

  Una ves instalados, reiniciar el Intellij para tener listo el ambiente.


### Ejecutando el programa ⚙️

#### Desde el entorno de desarrollo Intellij

* Si deseas ejecutar el programa _Ingresa a la clase "InvestmentSimulatorControllerRunner" y preciona el icono de ejecutar el cual se encuentra en la parte izquierda de la linea donde se inicializa la clase._

* Si deseas ejecutar el programa _Ingresa a la clase "InvestmentSimulatorControllerRunner", dar click derecho sobre la clase y seleccionar la opcion "Run InvestmentSimulatorControllerRunner.main()"._

## Construido con 🛠️

* [Git](https://git-scm.com/downloads) - El manejador de versiones
* [IntelliJ](https://www.jetbrains.com/es-es/idea/download/#section=windows) - El IDe De preferencia
* [Java 11](https://www.oracle.com/co/java/technologies/javase/javase-jdk8-downloads.html) - El JDK para correr el robot.
* [Gradle](https://gradle.org/install/) - El Gestor de dependencias
* [JavaFX](https://openjfx.io/) - El conjunto de paquetes graficos (Desarrollo de interfaz grafica).

## Condiciones de ejecucion 🕶️

Este programa se realizo teniendo en cuenta las condiciones planteadas en la prueba "https://sites.google.com/tyba.com.co/strange-automation-qa/tyba-automation-challenge" y se a establecido un rango de ingreso de dinero el cual por defecto esta de $0 a $300.000.000, adicionalmente a continuacion se plantea las condiciones usadas:

* Cuando no se ingrese el nombre, tipo de documento o monto se obtendra un error.

* Cuando se seleccion un tipo de documento diferente a cedula de ciudadania o cedula de extranjeria se obtendra un error.

* Cuando se ingrese un monto inferior a $0 se obtendra un error (Este monto es modificable).

* Cuando se ingrese un monto superior a $300.000.000 se obtendra un error (Este monto es modificable).

* Si el monto es inferior a $200.000 y superior al monto minimo que se puede ingresar se obtendra un error (Este monto es modificable).

* Si el monto es igual o superior a $200.000 e inferior a $5.000.0000 se recomendara el producto CDT (Estos montos son modificables).

* Si el monto es igual o superior a $5.000.000 e inferior a $25.000.0000 se recomendara el producto Acciones (Estos montos son modificables).

* Si el monto es igual o superior a $25.000.000 e igual o inferior a $300.000.0000 se recomendara el producto Fondo de inversion (Estos montos son modificables).

Los montos que anteriormente se mencionan como modificables se podran cambiar en la clase tipo Enum "DefinitionRangsInvestmentSimulator" la cual se encuentra en la siguiente ruta "src/main/java/com/tyba/primer_ejercicio/enums/DefinitionRangsInvestmentSimulator.java".
